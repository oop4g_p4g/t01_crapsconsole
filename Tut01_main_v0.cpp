////////////////////////////////////////////////////////////////////////
// 3DGD Tutorial 1: Simple C++ OO Dice Game simulator (version 0: to be modified)
// Hacky
////////////////////////////////////////////////////////////////////////

//--------include libraries
#include <ctime>		//for time used in random number routines
#include <iostream>		//for cin >> and cout <<
#include <string>		//for string routines
using namespace std;

//--------RandomNumberGenerator class
class RandomNumberGenerator
{
public:
	void seed();
	int getRandomValue(int) const;
};

void RandomNumberGenerator::seed() {
	//seed the random number generator from current system time
	//so that the numbers will be different every time
	srand(static_cast<unsigned>(time(0)));
}
int RandomNumberGenerator::getRandomValue(int max) const {
	//produce a random number in range [1..max]
	return (rand() % max) + 1;
}
//--------end of RandomNumberGenerator class

//--------Dice class
class Dice {
public:
	void init();
	void roll();
	int getFace() const;
private:
	int face_;		//the dice value
};

void Dice::init() {
	//initialise dice to an (invalid) value 
	face_ = 0;	// must be rolled before it can be used properly
}
int Dice::getFace() const {
	//return the current dice value
	// pre: must be rolled before it can be used properly
	return (face_);
}
void Dice::roll() {
	//produce a dice value in range [1..6]
	RandomNumberGenerator rng;
	face_ = rng.getRandomValue(6);
}
//--------end of Dice class

//--------Score class
class Score {
public:
	void init();
	int getAmount() const;
	void updateAmount(int);
private:
	int amount_;
};

void Score::init() {
	//initialise score's amount to 0
	amount_ = 0;
}
int Score::getAmount() const {
	//return score amount
	return amount_;
}
void Score::updateAmount(int value) {
	//increment when value>0, decrement otherwise
	amount_ += value;
}
//--------end of Score class

//--------Player class
class Player {
public:
	void init();
	string getName() const;
	Score getScore() const;
	void getPlayerDetails(Score&, string&) const;
	void updateScore(int);
	void viewScore() const;
private:
	string name_;	//player's name
	Score score_;	//player's current score
};

void Player::init() {
	//ask for and record  the name of the player and initialise its score
	cout << "ENTER THE PLAYER\'S NAME: ";
	cin >> name_;
	score_.init();	//no score yet
}
Score Player::getScore() const {
	return score_;
}
string Player::getName() const {
	return name_;
}
void Player::getPlayerDetails(Score& s, string& n) const {
	s = score_;
	n = name_;
}
void Player::updateScore(int amountWon) {
	//record how much player has won or lost
	score_.updateAmount(amountWon); 	//update score's amount accordingly
}
void Player::viewScore() const {
	//display the current player's results
	cout << "\nCURRENT SCORE FOR " << name_ << " IS: " << score_.getAmount();
}
//--------end of Player class

//--------DiceGame class
class DiceGame {
public:
	void init();
	int playGame();
	void displayRules() const;
private:
	RandomNumberGenerator rng_; 	//local object: to get random numbers
	Dice firstDice_, secondDice_;	//the two dice used in the game
	enum class GameStatus{ WON, LOST, CONTINUE}; //possible values for game state
	GameStatus status_; 	//state of a game: WIN, LOSE or CONTINUE
	int point_;	//sum value to be reached after the first throw
	void throwFirstTime();
	void throwAgain();
	bool isFinished() const;
	int rollDice();
	int calculateFinalScore() const;
	void displayAGameResult() const;
};

void DiceGame::init() {
	// initialise/reset the state of the game
	rng_.seed();
	firstDice_.init();
	secondDice_.init();
	status_ = GameStatus::CONTINUE;
	point_ = 0;
}
int DiceGame::playGame() {
	//play one game of dice & record results
	init();
	cout << "\nFirst throw: ";
	throwFirstTime();	//handle first throw
	while (!isFinished())	//while nobody wins or loses � keep rolling
	{
		cout << "\nNext throws: ";
		throwAgain();	//handle next throw
	}
	displayAGameResult();	//display results for that game
	return (calculateFinalScore());
}
bool DiceGame::isFinished() const {
	//check whether the game must continue
	return (status_ != GameStatus::CONTINUE);
}
void DiceGame::displayRules() const {
	//display the rules of the game
	cout << "\n_________GAME RULES__________";
	cout << "\nrolls 7 or 11 on first throw:  player wins!";
	cout << "\nrolls 2, 3 or 12 on first throw:  player loses!";
	cout << "\notherwise remember first sum as point and rolls again until...";
	cout << "\n   makes point again before rolling 7:  player wins! ";
	cout << "\n   rolls 7 before makes point again:  player loses! ";
	cout << "\n_____________________________";
	cout << "\n   NO COST! ";
	cout << "\n   POTENTIAL GAIN: 1 pound if player wins! ";
	cout << "\n   POTENTIAL LOSS: 1 pound if player loses! ";
}
int DiceGame::rollDice() {
	//"roll" two dice & produce their sum
	firstDice_.roll();	//roll the two dice
	secondDice_.roll();
	return (firstDice_.getFace() + secondDice_.getFace());	//return sum
}
void DiceGame::throwFirstTime() {
	//handle first throw
	const int diceSum(rollDice());	//first roll
	cout << "Player rolled: "
		<< firstDice_.getFace() 	//display dice values and their sum
		<< " & " << secondDice_.getFace()
		<< " giving " << diceSum;
	if ((diceSum == 7) || (diceSum == 11))
		status_ = GameStatus::WON; 	//player wins on first roll
	else
		if ((diceSum == 2) || (diceSum == 3) || (diceSum == 12))
			status_ = GameStatus::LOST; 	//player loses on first roll
		else  			//remember point and continue playing
		{
			status_ = GameStatus::CONTINUE;
			point_ = diceSum;
			cout << "\nPoint is : " << point_; 	//display point for game
		}
}
void DiceGame::throwAgain() {
	//handle next throw(s)
	const int diceSum(rollDice());	//subsequent roll(s)
	cout << "Player rolled: "
		<< firstDice_.getFace() 	//display dice values and their sum
		<< " & " << secondDice_.getFace()
		<< " giving " << diceSum;
	if (diceSum == point_)
		status_ = GameStatus::WON;	//player wins by making point
	else
		if (diceSum == 7)
			status_ = GameStatus::LOST;	//player lose by rolling 7
}
int DiceGame::calculateFinalScore() const{
	//calculate the amount won & lost by player
	if (status_ == GameStatus::WON) 	//no cost to play the game
		return 1;  	//one more game won by player
	else
		return -1;		//one more game lost by player
}
void DiceGame::displayAGameResult() const {
	//display results for one game
	if (status_ == GameStatus::WON)
		cout << "\nYOU WON!";
	else
		cout << "\nYOU LOST!";
}
//--------end of DiceGame class


//--------end of GameApplication class 
class GameApplication {
public:
	void init();
	void run();
private:
	DiceGame diceGame_;  	//the current game
	Player player_;	//the current player
	void displayMenu() const;
	char enterCommand() const;
	void processCommand(char);
	void quitProgram() const;
};

void GameApplication::init() {
	//initialise th state of the application
	diceGame_.init(); 	//reset state of the game (not strictly necessary (done each time the game is played)
	player_.init();		//reset state of player
}
void GameApplication::run() {
	char command;		//menu option selected by played
	displayMenu();		//display menu
	command = enterCommand();	//... and get command
	while (command != 'Q')	//while not quit
	{
		processCommand(command);	//perform command
		displayMenu();	//display menu again...
		command = enterCommand();	//... and read next command
	}
	quitProgram(); 		//display final result before stopping
}
void GameApplication::displayMenu() const {
	//display available commands
	cout << "\n\n	  _________DICE GAME_________";
	cout << "\n		  QUIT THE PROGRAM:   Q ";
	cout << "\n		  DISPLAY THE RULES:  R ";
	cout << "\n		  PLAY ONE GAME:      P ";
	cout << "\n		  DISPLAY SCORE:      S ";
	cout << "\n		  ____________________\n";
}
char GameApplication::enterCommand() const {
	//prompt the user for a command and read in a single letter
	char option;
	cout << "\nENTER YOUR COMMAND (Q, R, P, S): ";
	cin >> option;		//read in selected menu option
	option = toupper(option);	//convert to uppercase
	return option;
}

void GameApplication::processCommand(char com) {
	//check valid command & perform required action (except 'Quit'),
	//if invalid, ask for another one
	switch (com) {
	case 'R': //if option is "R"
		diceGame_.displayRules(); 	//display them
		break;			//else
	case 'P': //if option is "P"
		//play one game & record results
		player_.updateScore(diceGame_.playGame());
		break;			//else
	case 'S': //if option is "S"
		player_.viewScore();	//display current result for player
		break;	 		//else
	default:
		cout << "\nINVALID CHOICE, TRY AGAIN"; 	//display error
	}
}
void GameApplication::quitProgram() const {
	//display player's final results when command is 'Quit'
	Score score;
	string name;
	player_.getPlayerDetails(score, name);
	cout << "\nCURRENT PLAYER: " << name;
	const int finalScoreValue(score.getAmount());
	if (finalScoreValue > 0)   	//player made a "profit"
		cout << "\nYOU ARE LUCKY! YOU WON " << finalScoreValue;
	else
		if (finalScoreValue < 0)  	//player made a loss
			cout << "\nYOU ARE UNLUCKY! YOU LOST " << -finalScoreValue;
		else				//player broke even
			cout << "\nBREAK EVEN !";
	cout << "\nEND OF SESSION !\n";
}
//--------end of GameApplication class

//---------------------------------------------------------------------------
int main()
{
	//create the application
	GameApplication game;
	//initialise it
	game.init();
	//run it
	game.run();

	system("pause");
	return 0;
}

